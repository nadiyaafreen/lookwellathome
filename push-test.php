<?php

error_reporting(-1);
ini_set('display_errors', 'On');

public function iOS($data, $devicetoken) {
	$deviceToken = $devicetoken;
	$ctx = stream_context_create();
	// ck.pem is your certificate file
	stream_context_set_option($ctx, 'ssl', 'local_cert', 'wp-content/uploads/2019/03/pushcertDev.pem');
	stream_context_set_option($ctx, 'ssl', 'passphrase', 'vishal');
	// Open a connection to the APNS server
	$fp = stream_socket_client(
		'ssl://gateway.sandbox.push.apple.com:2195', $err,
		$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
	if (!$fp)
		exit("Failed to connect: $err $errstr" . PHP_EOL);
	// Create the payload body
	$body['aps'] = array(
		'alert' => array(
		    'title' => 'hello',
            'body' => 'test body',
		 ),
		'sound' => 'default'
	);
	// Encode the payload as JSON
	$payload = json_encode($body);
	// Build the binary notification
	$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
	// Send it to the server
	$result = fwrite($fp, $msg, strlen($msg));
	
	// Close the connection to the server
	fclose($fp);
	if (!$result)
		return 'Message not delivered' . PHP_EOL;
	else
		return 'Message successfully delivered' . PHP_EOL;
}

$deviceToken = 'e75132a81623266a58d7f912f0830c4ddc8b41b7abea91d4894b58f6526ea6fb';

	$msg_payload = array (
		'mtitle' => 'Test push notification title',
		'mdesc' => 'Test push notification body',
	);

iOS($msg_payload, $deviceToken);

?>
