<div class="wrap">
<h1>Enter your item purchase code to get Premium Support</h1>
<p><a href="https://ninjateam.org/can-find-item-purchase-code/" target="_blank">Where can I find my item purchase code?</a></p>
    <form action="" class="njt-check-purchase-frm" method="POST">
        <ul class="njt-check-purchase-wrap">
            <input type="hidden" name="nonce" value="<?php echo $nonce; ?>" />
            <li>
                <input type="text" name="njt-check-purchase-code" value="" id="njt-check-purchase-code" required="required" class="regular-text" placeholder="<?php _e('Enter your purchase code', NJT_FB_MESS_I18N); ?>" />
            </li>
            <li>
                <?php submit_button(__('Submit', NJT_FB_MESS_I18N)); ?>
            </li>
        </ul>
        <div class="njt-check-purchase-result"></div>
    </form>
</div>