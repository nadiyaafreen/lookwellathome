msgid ""
msgstr ""
"Project-Id-Version: WooCommerce Easy Booking\n"
"POT-Creation-Date: 2019-01-01 17:43+0100\n"
"PO-Revision-Date: 2019-01-01 17:43+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.5.7\n"
"X-Poedit-KeywordsList: _;gettext;gettext_noop;__;_e;_n:1,2;esc_html_e;"
"esc_html__\n"
"X-Poedit-Basepath: .\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Poedit-SearchPath-0: .\n"

#: woocommerce-easy-booking.php:211
#: includes/settings/class-wceb-settings-page.php:25
#: includes/settings/class-wceb-settings-page.php:26
msgid "Settings"
msgstr ""

#: includes/class-wceb-ajax.php:632
msgid "Please choose valid dates"
msgstr ""

#: includes/class-wceb-ajax.php:635 includes/class-wceb-cart.php:89
#: includes/class-wceb-cart.php:100
msgid "Please choose two dates"
msgstr ""

#: includes/class-wceb-ajax.php:638
msgid "Please select product option"
msgstr ""

#: includes/class-wceb-ajax.php:641
msgid ""
"Please choose the quantity of items you wish to add to your cart&hellip;"
msgstr ""

#: includes/class-wceb-ajax.php:644 includes/class-wceb-cart.php:82
msgid "You can only select one date"
msgstr ""

#: includes/class-wceb-ajax.php:647 includes/class-wceb-cart.php:77
#: includes/class-wceb-cart.php:98
msgid "Please choose a date"
msgstr ""

#: includes/class-wceb-ajax.php:690 includes/class-wceb-ajax.php:694
msgid "Sorry there was a problem. Please try again."
msgstr ""

#: includes/class-wceb-ajax.php:784
msgid "week"
msgid_plural "weeks"
msgstr[0] ""
msgstr[1] ""

#: includes/class-wceb-ajax.php:786
msgid "night"
msgid_plural "nights"
msgstr[0] ""
msgstr[1] ""

#: includes/class-wceb-ajax.php:786
msgid "day"
msgid_plural "days"
msgstr[0] ""
msgstr[1] ""

#: includes/class-wceb-ajax.php:790
#, php-format
msgid "Total booking duration: %s %s"
msgstr ""

#: includes/class-wceb-ajax.php:799
#, php-format
msgid "Average price %s: %s"
msgstr ""

#: includes/class-wceb-assets.php:143 includes/class-wceb-cart.php:300
#: includes/class-wceb-checkout.php:164 includes/class-wceb-checkout.php:203
#: includes/class-wceb-product-view.php:52
#: includes/admin/class-wceb-order.php:38
#: includes/admin/views/html-wceb-add-order-item-meta.php:55
#: includes/admin/views/html-wceb-edit-order-item-meta.php:56
#: includes/reports/class-wceb-list-reports.php:55
#: includes/reports/views/html-wceb-reports-filters.php:15
msgid "Start"
msgstr ""

#: includes/class-wceb-assets.php:144 includes/class-wceb-cart.php:301
#: includes/class-wceb-checkout.php:165 includes/class-wceb-checkout.php:204
#: includes/class-wceb-product-view.php:53
#: includes/admin/class-wceb-order.php:39
#: includes/admin/views/html-wceb-add-order-item-meta.php:57
#: includes/admin/views/html-wceb-edit-order-item-meta.php:58
#: includes/reports/class-wceb-list-reports.php:56
#: includes/reports/views/html-wceb-reports-filters.php:17
msgid "End"
msgstr ""

#: includes/class-wceb-product-archive-view.php:34
msgid "Select date(s)"
msgstr ""

#: includes/wceb-product-functions.php:227
msgid " / week"
msgstr ""

#: includes/wceb-product-functions.php:232
#, php-format
msgid " / %s night"
msgid_plural " / %s nights"
msgstr[0] ""
msgstr[1] ""

#: includes/wceb-product-functions.php:234
#, php-format
msgid " / %s day"
msgid_plural " / %s days"
msgstr[0] ""
msgstr[1] ""

#: includes/wceb-product-functions.php:239
msgid " / night"
msgstr ""

#: includes/wceb-product-functions.php:243
msgid " / day"
msgstr ""

#: includes/admin/class-wceb-admin-assets.php:72
#: includes/admin/class-wceb-admin-assets.php:82
#: includes/admin/class-wceb-admin-assets.php:93
#: includes/admin/views/wceb-html-product-booking-options.php:72
#: includes/admin/views/wceb-html-product-booking-options.php:88
#: includes/admin/views/wceb-html-variation-booking-options.php:71
#: includes/admin/views/wceb-html-variation-booking-options.php:84
msgid "days"
msgstr ""

#: includes/admin/class-wceb-admin-assets.php:76
#: includes/admin/class-wceb-admin-assets.php:94
msgid "weeks"
msgstr ""

#: includes/admin/class-wceb-admin-assets.php:79
#: includes/admin/class-wceb-admin-assets.php:95
msgid "custom period"
msgstr ""

#: includes/admin/class-wceb-admin-product-settings.php:52
#: includes/admin/class-wceb-admin-product-settings.php:78
msgid "Bookable"
msgstr ""

#: includes/admin/class-wceb-admin-product-settings.php:53
msgid ""
"Bookable products can be rent or booked on a daily/weekly/custom schedule"
msgstr ""

#: includes/admin/class-wceb-admin-product-settings.php:108
#: includes/reports/views/html-wceb-reports.php:6
msgid "Bookings"
msgstr ""

#: includes/admin/class-wceb-admin-product-settings.php:158
#: includes/admin/class-wceb-admin-product-settings.php:175
msgid "Multiply addon cost by booking duration (bookable products only)?"
msgstr ""

#: includes/admin/class-wceb-admin-product-settings.php:160
#: includes/admin/class-wceb-admin-product-settings.php:177
msgid "Multiply addon cost by booking duration?"
msgstr ""

#: includes/admin/class-wceb-admin-product-settings.php:291
msgid "Minimum booking duration must be inferior to maximum booking duration"
msgstr ""

#: includes/admin/views/html-wceb-add-order-item-meta.php:47
#: includes/admin/views/html-wceb-edit-order-item-meta.php:48
#: includes/admin/views/html-wceb-order-item-meta.php:32
#: includes/reports/views/html-wceb-reports-filters.php:13
msgid "Booking status"
msgstr ""

#: includes/admin/views/html-wceb-add-order-item-meta.php:54
#: includes/admin/views/html-wceb-edit-order-item-meta.php:55
#: includes/reports/views/html-wceb-reports-filters.php:14
msgid "Pending"
msgstr ""

#: includes/admin/views/html-wceb-add-order-item-meta.php:56
#: includes/admin/views/html-wceb-edit-order-item-meta.php:57
#: includes/reports/views/html-wceb-reports-filters.php:16
msgid "Processing"
msgstr ""

#: includes/admin/views/html-wceb-add-order-item-meta.php:58
#: includes/admin/views/html-wceb-edit-order-item-meta.php:59
#: includes/reports/views/html-wceb-reports-filters.php:18
msgid "Completed"
msgstr ""

#: includes/admin/views/wceb-html-notice-addons.php:11
#, php-format
msgid ""
"Thanks for installing Easy Booking! If you want more features, %scheck the "
"add-ons%s!"
msgstr ""

#: includes/admin/views/wceb-html-notice-update-settings.php:11
#, php-format
msgid ""
"Thanks for updating Easy Booking! This update contains important changes, "
"please %smake sure your settings are correctly saved.%s"
msgstr ""

#: includes/admin/views/wceb-html-notice-update-settings.php:14
msgid ""
"To match wordpress.org internationalization rules, the plugin's textdomain "
"has been modified."
msgstr ""

#: includes/admin/views/wceb-html-notice-update-settings.php:17
msgid ""
"If you have .mo and .po files, please rename them this way to get your "
"translations back:"
msgstr ""

#: includes/admin/views/wceb-html-notice-update-settings.php:23
msgid "Replace \"fr_FR\" by your own language code."
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:24
#: includes/admin/views/wceb-html-variation-booking-options.php:15
#: includes/settings/includes/wceb-general-settings.php:38
msgid "Number of dates to select"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:26
#: includes/admin/views/wceb-html-variation-booking-options.php:16
msgid "Choose whether to have one or two date(s) to select for this product."
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:29
#: includes/admin/views/wceb-html-product-booking-options.php:46
#: includes/admin/views/wceb-html-product-booking-options.php:59
#: includes/admin/views/wceb-html-product-booking-options.php:76
#: includes/admin/views/wceb-html-product-booking-options.php:92
#: includes/admin/views/wceb-html-product-booking-options.php:110
msgid "Same as global settings"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:30
#: includes/admin/views/wceb-html-variation-booking-options.php:23
#: includes/settings/class-wceb-settings-general.php:196
msgid "One"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:31
#: includes/admin/views/wceb-html-variation-booking-options.php:26
#: includes/settings/class-wceb-settings-general.php:197
msgid "Two"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:41
#: includes/admin/views/wceb-html-variation-booking-options.php:37
#: includes/settings/includes/wceb-general-settings.php:46
msgid "Booking duration"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:43
#: includes/admin/views/wceb-html-variation-booking-options.php:38
msgid ""
"The booking duration of your products. Daily, weekly or a custom period (e."
"g. 28 days for a monthly booking). The price will be applied to the whole "
"period."
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:47
#: includes/admin/views/wceb-html-variation-booking-options.php:45
#: includes/settings/class-wceb-settings-general.php:236
msgid "Daily"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:48
#: includes/admin/views/wceb-html-variation-booking-options.php:48
#: includes/settings/class-wceb-settings-general.php:237
msgid "Weekly"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:49
#: includes/admin/views/wceb-html-variation-booking-options.php:51
#: includes/settings/class-wceb-settings-general.php:238
msgid "Custom"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:57
#: includes/admin/views/wceb-html-variation-booking-options.php:59
msgid "Custom booking duration (days)"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:72
#: includes/admin/views/wceb-html-variation-booking-options.php:71
#: includes/settings/includes/wceb-general-settings.php:62
msgid "Minimum booking duration"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:74
msgid ""
"The minimum number of days / weeks / custom period to book. Leave zero to "
"set no duration limit. Leave empty to use the global settings."
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:88
#: includes/admin/views/wceb-html-variation-booking-options.php:84
#: includes/settings/includes/wceb-general-settings.php:70
msgid "Maximum booking duration"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:90
msgid ""
"The maximum number of days / weeks / custom period to book. Leave zero to "
"set no duration limit. Leave empty to use the global settings."
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:106
msgid "First available date (day)"
msgstr ""

#: includes/admin/views/wceb-html-product-booking-options.php:108
msgid ""
"First available date, relative to the current day. I.e. : today + 5 days. "
"Leave zero for the current day. Leave empty to use the global settings."
msgstr ""

#: includes/admin/views/wceb-html-variation-booking-options.php:20
#: includes/admin/views/wceb-html-variation-booking-options.php:42
#: includes/admin/views/wceb-html-variation-booking-options.php:64
#: includes/admin/views/wceb-html-variation-booking-options.php:77
#: includes/admin/views/wceb-html-variation-booking-options.php:90
#: includes/admin/views/wceb-html-variation-booking-options.php:105
msgid "Same as parent"
msgstr ""

#: includes/admin/views/wceb-html-variation-booking-options.php:72
msgid ""
"The minimum number of days / weeks / custom period to book. Enter zero to "
"set no duration limit or leave blank to use the parent product's booking "
"options or the global settings."
msgstr ""

#: includes/admin/views/wceb-html-variation-booking-options.php:85
msgid ""
"The maximum number of days / weeks / custom period to book. Enter zero to "
"set no duration limit or leave blank to use the parent product's booking "
"options or the global settings."
msgstr ""

#: includes/admin/views/wceb-html-variation-booking-options.php:99
#: includes/settings/includes/wceb-general-settings.php:78
msgid "First available date"
msgstr ""

#: includes/admin/views/wceb-html-variation-booking-options.php:100
msgid ""
"First available date, relative to today. I.e. : today + 5 days. Enter 0 for "
"today or leave blank to use the parent product's booking options or the "
"global settings."
msgstr ""

#: includes/reports/class-wceb-list-reports.php:18
msgid "Report"
msgstr ""

#: includes/reports/class-wceb-list-reports.php:19
#: includes/reports/class-wceb-reports.php:21
#: includes/reports/class-wceb-reports.php:22
msgid "Reports"
msgstr ""

#: includes/reports/class-wceb-list-reports.php:52
msgid "Status"
msgstr ""

#: includes/reports/class-wceb-list-reports.php:53
msgid "Order"
msgstr ""

#: includes/reports/class-wceb-list-reports.php:54
msgid "Product"
msgstr ""

#: includes/reports/class-wceb-list-reports.php:57
msgid "Quantity booked"
msgstr ""

#: includes/reports/class-wceb-list-reports.php:166
msgid "Imported booking"
msgstr ""

#: includes/reports/class-wceb-reports.php:92
msgid "To see \"Completed\" bookings, use the booking status filter below."
msgstr ""

#: includes/reports/views/html-wceb-reports-filters.php:26
msgid "Search for a product&hellip;"
msgstr ""

#: includes/settings/class-wceb-addons-page.php:25
#: includes/settings/class-wceb-addons-page.php:26
msgid "Add-ons"
msgstr ""

#: includes/settings/class-wceb-settings-appearance.php:66
msgid "Appearance settings"
msgstr ""

#: includes/settings/class-wceb-settings-appearance.php:158
msgid "Customize the calendar so it looks great with your theme!"
msgstr ""

#: includes/settings/class-wceb-settings-appearance.php:158
msgid "For better rendering, prefer a light background and a dark text color."
msgstr ""

#: includes/settings/class-wceb-settings-appearance.php:173
msgid "Default"
msgstr ""

#: includes/settings/class-wceb-settings-appearance.php:174
msgid "Classic"
msgstr ""

#: includes/settings/class-wceb-settings-functions.php:229
msgid "Add"
msgstr ""

#: includes/settings/class-wceb-settings-functions.php:273
msgid "Delete"
msgstr ""

#: includes/settings/class-wceb-settings-general.php:71
#: includes/settings/includes/wceb-general-settings.php:15
msgid "General settings"
msgstr ""

#: includes/settings/class-wceb-settings-general.php:176
msgid ""
"Check to make all your products bookable. If checked, any new or modified "
"product will be automatically bookable."
msgstr ""

#: includes/settings/class-wceb-settings-general.php:193
msgid "Choose the number of dates to select. Customizable at product level."
msgstr ""

#: includes/settings/class-wceb-settings-general.php:214
msgid ""
"Choose whether to book your products by day or by night (i.e. 5 days = 4 "
"nights)."
msgstr ""

#: includes/settings/class-wceb-settings-general.php:216
msgid "Days"
msgstr ""

#: includes/settings/class-wceb-settings-general.php:217
msgid "Nights"
msgstr ""

#: includes/settings/class-wceb-settings-general.php:233
msgid ""
"Choose booking duration for two dates selection. Customizable at product "
"level."
msgstr ""

#: includes/settings/class-wceb-settings-general.php:255
msgid ""
"Used only for products with \"Custom\" booking duration. Customizable at "
"product level."
msgstr ""

#: includes/settings/class-wceb-settings-general.php:278
msgid ""
"Set a minimum booking duration. Leave 0 or empty to set no minmum. "
"Customizable at product level."
msgstr ""

#: includes/settings/class-wceb-settings-general.php:300
msgid ""
"Set a maximum booking duration. Leave 0 or empty to set no maximum. "
"Customizable at product level."
msgstr ""

#: includes/settings/class-wceb-settings-general.php:322
msgid ""
"Set the first available date, relative to the current day. Leave 0 or empty "
"to keep the current day. Customizable at product level."
msgstr ""

#: includes/settings/class-wceb-settings-general.php:343
msgid ""
"Set the last available date, relative to the current day. Max: 3650 days (10 "
"years)."
msgstr ""

#: includes/settings/class-wceb-settings-page.php:47
#: includes/settings/class-wceb-settings-page.php:48
msgid "Network Settings"
msgstr ""

#: includes/settings/class-wceb-settings-statuses.php:67
msgid "Booking statuses settings"
msgstr ""

#: includes/settings/class-wceb-settings-statuses.php:162
msgid ""
"These booking statuses are for information and organization purposes only, "
"and are not related to WooCommerce order statuses."
msgstr ""

#: includes/settings/class-wceb-settings-statuses.php:164
msgid "They follow this pattern:"
msgstr ""

#: includes/settings/class-wceb-settings-statuses.php:166
msgid "Pending: "
msgstr ""

#: includes/settings/class-wceb-settings-statuses.php:166
msgid "Booking hasn't started yet."
msgstr ""

#: includes/settings/class-wceb-settings-statuses.php:167
msgid "Start: "
msgstr ""

#: includes/settings/class-wceb-settings-statuses.php:167
msgid ""
"Booking starts. If you need time to prepare or deliver your items, keep this "
"status a few days before the actual booking start date."
msgstr ""

#: includes/settings/class-wceb-settings-statuses.php:168
msgid "Processing..."
msgstr ""

#: includes/settings/class-wceb-settings-statuses.php:169
msgid "End: "
msgstr ""

#: includes/settings/class-wceb-settings-statuses.php:169
msgid ""
"Booking ends. If you need time to retrieve or prepare your items for another "
"booking, keep this status a few days after the actual booking end date."
msgstr ""

#: includes/settings/class-wceb-settings-statuses.php:170
msgid "Completed: "
msgstr ""

#: includes/settings/class-wceb-settings-statuses.php:170
msgid "Booking is over."
msgstr ""

#: includes/settings/class-wceb-settings-statuses.php:173
msgid ""
"You can choose to change them manually or automatically and then easily "
"track your bookings in the \"Reports\" page."
msgstr ""

#: includes/settings/class-wceb-settings-statuses.php:190
msgid ""
"If set to \"Automatically\", \"Start\" status will be set on booking start "
"date."
msgstr ""

#: includes/settings/class-wceb-settings-statuses.php:192
#: includes/settings/class-wceb-settings-statuses.php:233
#: includes/settings/class-wceb-settings-statuses.php:253
#: includes/settings/class-wceb-settings-statuses.php:293
msgid "Automatically"
msgstr ""

#: includes/settings/class-wceb-settings-statuses.php:193
#: includes/settings/class-wceb-settings-statuses.php:234
#: includes/settings/class-wceb-settings-statuses.php:254
#: includes/settings/class-wceb-settings-statuses.php:294
msgid "Manually"
msgstr ""

#: includes/settings/class-wceb-settings-statuses.php:210
msgid "Day(s) before start date. Only if automatically is set."
msgstr ""

#: includes/settings/class-wceb-settings-statuses.php:231
msgid ""
"If set to \"Automatically\", \"Processing\" status will start the day after "
"booking start date."
msgstr ""

#: includes/settings/class-wceb-settings-statuses.php:251
msgid ""
"If set to \"Automatically\", \"End\" status will be set on booking end date."
msgstr ""

#: includes/settings/class-wceb-settings-statuses.php:271
msgid "Day(s) after end date. Only if automatically is set."
msgstr ""

#: includes/settings/includes/wceb-appearance-settings.php:15
#: includes/settings/views/html-wceb-settings-page.php:5
#: includes/settings/views/html-wceb-settings.php:5
msgid "Appearance"
msgstr ""

#: includes/settings/includes/wceb-appearance-settings.php:22
msgid "Calendar theme"
msgstr ""

#: includes/settings/includes/wceb-appearance-settings.php:30
msgid "Background color"
msgstr ""

#: includes/settings/includes/wceb-appearance-settings.php:38
msgid "Main color"
msgstr ""

#: includes/settings/includes/wceb-appearance-settings.php:46
msgid "Text color"
msgstr ""

#: includes/settings/includes/wceb-emails-settings.php:15
msgid "\"Start\" status settings"
msgstr ""

#: includes/settings/includes/wceb-emails-settings.php:22
msgid "Change item start booking status:"
msgstr ""

#: includes/settings/includes/wceb-emails-settings.php:30
msgid "If automatically, change item start booking status:"
msgstr ""

#: includes/settings/includes/wceb-emails-settings.php:38
msgid "\"Processing\" status settings"
msgstr ""

#: includes/settings/includes/wceb-emails-settings.php:45
msgid "Change item processing booking status:"
msgstr ""

#: includes/settings/includes/wceb-emails-settings.php:53
msgid "\"End\" status settings"
msgstr ""

#: includes/settings/includes/wceb-emails-settings.php:60
msgid "\"Completed\" status settings"
msgstr ""

#: includes/settings/includes/wceb-emails-settings.php:67
msgid "Change item completed booking status:"
msgstr ""

#: includes/settings/includes/wceb-emails-settings.php:75
msgid "If automatically, change item completed booking status:"
msgstr ""

#: includes/settings/includes/wceb-general-settings.php:22
msgid "Calculation mode"
msgstr ""

#: includes/settings/includes/wceb-general-settings.php:30
msgid "Make all products bookable?"
msgstr ""

#: includes/settings/includes/wceb-general-settings.php:54
msgid "Custom booking duration"
msgstr ""

#: includes/settings/includes/wceb-general-settings.php:86
msgid "Last available date"
msgstr ""

#: includes/settings/includes/wceb-general-settings.php:94
msgid "First weekday"
msgstr ""

#: includes/settings/views/html-wceb-addons-page.php:9
#: includes/settings/views/html-wceb-addons.php:8
msgid "WooCommerce Easy Booking Add-ons"
msgstr ""

#: includes/settings/views/html-wceb-addons-page.php:16
#: includes/settings/views/html-wceb-addons.php:15
msgid "Manage availabilities of your bookable products."
msgstr ""

#: includes/settings/views/html-wceb-addons-page.php:21
msgid "Set discounts or surcharges depending on booking duration."
msgstr ""

#: includes/settings/views/html-wceb-addons-page.php:26
msgid "Disable days or dates on your products' booking schedules."
msgstr ""

#: includes/settings/views/html-wceb-addons-page.php:31
#: includes/settings/views/html-wceb-addons.php:38
msgid "Set different prices depending on a day, date or daterange."
msgstr ""

#: includes/settings/views/html-wceb-addons-page.php:36
msgid "Syncronize your bookings with Google Calendar."
msgstr ""

#: includes/settings/views/html-wceb-addons-page.php:62
#: includes/settings/views/html-wceb-addons.php:62
msgid "Learn more"
msgstr ""

#: includes/settings/views/html-wceb-addons-page.php:66
#: includes/settings/views/html-wceb-addons.php:66
msgid "Installed"
msgstr ""

#: includes/settings/views/html-wceb-addons-page.php:69
#: includes/settings/views/html-wceb-addons.php:69
msgid "Documentation"
msgstr ""

#: includes/settings/views/html-wceb-addons-page.php:72
#: includes/settings/views/html-wceb-addons.php:72
msgid "Support"
msgstr ""

#: includes/settings/views/html-wceb-addons.php:22
msgid "Set discounts or surcharges depending on booking durations."
msgstr ""

#: includes/settings/views/html-wceb-addons.php:30
msgid "Disable days or dates on your products booking schedules."
msgstr ""

#: includes/settings/views/html-wceb-network-settings-page.php:5
#: includes/settings/views/html-wceb-network-settings.php:5
msgid "Network settings for WooCommerce Easy Booking"
msgstr ""

#: includes/settings/views/html-wceb-settings-page.php:4
#: includes/settings/views/html-wceb-settings.php:4
msgid "General "
msgstr ""

#: includes/settings/views/html-wceb-settings-page.php:6
#: includes/settings/views/html-wceb-settings.php:6
msgid "Booking statuses"
msgstr ""

#: includes/views/wceb-html-product-view.php:27
msgid "Clear dates"
msgstr ""
