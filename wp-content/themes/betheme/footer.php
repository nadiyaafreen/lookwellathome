<?php
/**
 * The template for displaying the footer.
 *
 * @package Betheme
 * @author Muffin group
 * @link https://muffingroup.com
 */


$back_to_top_class = mfn_opts_get('back-top-top');

if( $back_to_top_class == 'hide' ){
	$back_to_top_position = false;
} elseif( strpos( $back_to_top_class, 'sticky' ) !== false ){
	$back_to_top_position = 'body';
} elseif( mfn_opts_get('footer-hide') == 1 ){
	$back_to_top_position = 'footer';
} else {
	$back_to_top_position = 'copyright';
}

?>

<?php do_action( 'mfn_hook_content_after' ); ?>

<?php if( 'hide' != mfn_opts_get( 'footer-style' ) ): ?>
	<!-- #Footer -->
	<footer id="Footer" class="clearfix">

		<?php if ( $footer_call_to_action = mfn_opts_get('footer-call-to-action') ): ?>
		<div class="footer_action">
			<div class="container">
				<div class="column one column_column">
					<?php echo do_shortcode( $footer_call_to_action ); ?>
				</div>
			</div>
		</div>
		<?php endif; ?>

		<?php
			$sidebars_count = 0;
			for( $i = 1; $i <= 5; $i++ ){
				if ( is_active_sidebar( 'footer-area-'. $i ) ) $sidebars_count++;
			}

			if( $sidebars_count > 0 ){

				$footer_style = '';

				if( mfn_opts_get( 'footer-padding' ) ){
					$footer_style .= 'padding:'. mfn_opts_get( 'footer-padding' ) .';';
				}

				echo '<div class="widgets_wrapper" style="'. $footer_style .'">';
					echo '<div class="container">';

						if( $footer_layout = mfn_opts_get( 'footer-layout' ) ){
							// Theme Options

							$footer_layout 	= explode( ';', $footer_layout );
							$footer_cols 	= $footer_layout[0];

							for( $i = 1; $i <= $footer_cols; $i++ ){
								if ( is_active_sidebar( 'footer-area-'. $i ) ){
									echo '<div class="column '. $footer_layout[$i] .'">';
										dynamic_sidebar( 'footer-area-'. $i );
									echo '</div>';
								}
							}

						} else {
							// Default - Equal Width

							$sidebar_class = '';
							switch( $sidebars_count ){
								case 2: $sidebar_class = 'one-second'; break;
								case 3: $sidebar_class = 'one-third'; break;
								case 4: $sidebar_class = 'one-fourth'; break;
								case 5: $sidebar_class = 'one-fifth'; break;
								default: $sidebar_class = 'one';
							}

							for( $i = 1; $i <= 5; $i++ ){
								if ( is_active_sidebar( 'footer-area-'. $i ) ){
									echo '<div class="column '. $sidebar_class .'">';
										dynamic_sidebar( 'footer-area-'. $i );
									echo '</div>';
								}
							}

						}

					echo '</div>';
				echo '</div>';
			}
		?>

		<?php if( mfn_opts_get('footer-hide') != 1 ): ?>

			<div class="footer_copy">
				<div class="container">
					<div class="column one">

						<?php
							if( $back_to_top_position == 'copyright' ){
								echo '<a id="back_to_top" class="button button_js" href=""><i class="icon-up-open-big"></i></a>';
							}
						?>

						<!-- Copyrights -->
						<div class="copyright">
							<?php
								if( mfn_opts_get('footer-copy') ){
									echo do_shortcode( mfn_opts_get('footer-copy') );
								} else {
									echo '&copy; '. date( 'Y' ) .' '. get_bloginfo( 'name' ) .'. All Rights Reserved. <a target="_blank" rel="nofollow" href="https://muffingroup.com">Muffin group</a>';
								}
							?>
						</div>

						<?php
							if( has_nav_menu( 'social-menu-bottom' ) ){
								mfn_wp_social_menu_bottom();
							} else {
								get_template_part( 'includes/include', 'social' );
							}
						?>

					</div>
				</div>
			</div>

		<?php endif; ?>

		<?php
			if( $back_to_top_position == 'footer' ){
				echo '<a id="back_to_top" class="button button_js in_footer" href=""><i class="icon-up-open-big"></i></a>';
			}
		?>

	</footer>
<?php endif; ?>

</div><!-- #Wrapper -->

<?php
	// Responsive | Side Slide
	if( mfn_opts_get( 'responsive-mobile-menu' ) ){
		get_template_part( 'includes/header', 'side-slide' );
	}
?>

<?php
	if( $back_to_top_position == 'body' ){
		echo '<a id="back_to_top" class="button button_js '. $back_to_top_class .'" href=""><i class="icon-up-open-big"></i></a>';
	}
?>

<?php if( mfn_opts_get('popup-contact-form') ): ?>
	<div id="popup_contact">
		<a class="button button_js" href="#"><i class="<?php mfn_opts_show( 'popup-contact-form-icon', 'icon-mail-line' ); ?>"></i></a>
		<div class="popup_contact_wrapper">
			<?php echo do_shortcode( mfn_opts_get('popup-contact-form') ); ?>
			<span class="arrow"></span>
		</div>
	</div>
<?php endif; ?>

<?php do_action( 'mfn_hook_bottom' ); ?>

<!-- wp_footer() -->
<?php wp_footer(); ?>
<script type="text/javascript">
	jQuery.noConflict();
	jQuery(document).ready(function () {
	jQuery( ".myList" ).each(function() {

	  var x=5;
	  var id = jQuery(this).data('list');
	  size_li = jQuery("#myList-"+id+" li").size();
	  jQuery('#myList-'+id+' li:lt('+x+')').show();
	});
		
    
    //jQuery('#myList li:lt('+x+')').show();
    jQuery('.loadMore').click(function () {
    	var id = jQuery(this).data('id');
    	size_li = jQuery("#myList-"+id+" li").size();
        x= (x+5 <= size_li) ? x+5 : size_li;
        console.log(x);
        jQuery('#myList-'+id+' li:lt('+x+')').show();
         jQuery('#showLess-'+id).show();
        if(x == size_li){
            jQuery('#loadMore-'+id).hide();
        }
    });
    jQuery('.showLess').click(function () {
    	var id = jQuery(this).data('id');
    	size_li = jQuery("#myList-"+id+" li").size();
    	console.log(x+"---");
        x=(x-5<0) ? 5 : (x-5);
        if(x<=5){
        	x=5;
        }
        console.log(x);
        jQuery('#myList-'+id+' li').not(':lt('+x+')').hide();
        jQuery('#loadMore-'+id).show();
        jQuery('#showLess-'+id).show();
        if(x == 5){
            jQuery('#showLess-'+id).hide();
        }
    });

	
});
	function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}

jQuery(document).ready(function () {
    size_li = jQuery("#myList li").size();
    x=5;
    jQuery('#myList li:lt('+x+')').show();
    jQuery('#loadMore').click(function () {
        x= (x+5 <= size_li) ? x+5 : size_li;
        jQuery('#myList li:lt('+x+')').show();
         jQuery('#showLess').show();
        if(x == 5){
            jQuery('#loadMore').hide();
        }
    });
    jQuery('#showLess').click(function () {
        x=(x-5<0) ? 5 : x-5;
        jQuery('#myList li').not(':lt('+x+')').hide();
        jQuery('#loadMore').show();
        jQuery('#showLess').show();
        if(x == 5){
            jQuery('#showLess').hide();
        }
    });
});

    
</script>
<script type="text/javascript">
	
jQuery(document).ready(function () {
var wpcf7Elm = document.querySelector( '.wpcf7' );
 if(wpcf7Elm){
        wpcf7Elm.addEventListener( 'wpcf7mailsent', function( event ) {
        jQuery("#pum-4937").remove();
        Swal.fire({
        position: 'center',
        type: 'success',
        title: 'Thank You',
        text:'We will get back to you very soon',
        showConfirmButton: false,
        timer: 3000
        });
        
    }, false );
 }
});
</script>
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5cf6a9ffb534676f32ad5817/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
</body>
</html>
