<?php do_action( 'woocommerce_before_single_product' );?>
<div  class="product">
	<h2><?php echo get_the_title();?></h2>
<ul class="myList" id="myList-<?php echo get_the_ID();?>" data-list="<?php echo get_the_ID();?>">
<?php

	$product = new WC_Product_Variable( get_the_ID() );
    $variations = $product->get_available_variations();
    
    foreach ($variations as $variation) { 
    	$json = array($variation);
    	
?>
		<li>
		<form class="variations_form cart margin0" action="<?php the_permalink();?>" method="post" enctype="multipart/form-data" data-product_id="<?php echo get_the_id();?>" data-product_variations='<?php echo json_encode($json);?>'>
			<table class="new-cart-form">
				<tr>
					<?php foreach ($variation['attributes'] as $key => $value) {?>
						<td><?php echo $variation['attributes'][$key];?></td>
					<?php } ?>
					
					
					<td><?php echo $variation['price_html'];?></td>
					<td><button type="submit" class="single_add_to_cart_button button alt">Add to cart</button></td>
				</tr>
			</table>
			<input type="hidden" name="add-to-cart" value="<?php echo get_the_id();?>">
			<input type="hidden" name="product_id" value="<?php echo get_the_id();?>">
			<input type="hidden" name="variation_id" class="variation_id" value="<?php echo $variation['variation_id'];?>">
		</form>
	</li>
<?php } ?>
</ul>
</div>
<div data-id="<?php echo get_the_ID();?>" class="loadMore" id="loadMore-<?php echo get_the_ID();?>">Load more Options</div>
<div data-id="<?php echo get_the_ID();?>" class="showLess" id="showLess-<?php echo get_the_ID();?>">Show less Options</div>
<?php
do_action( 'woocommerce_after_single_product' ); ?>
<script type="text/javascript">
	(function ($) {
 
    $(document).on('click', '.single_add_to_cart_button', function (e) {
        e.preventDefault();
 
        var $thisbutton = $(this),
                $form = $thisbutton.closest('form.cart'),
                id = $thisbutton.val(),
                product_qty = $form.find('input[name=quantity]').val() || 1,
                product_id = $form.find('input[name=product_id]').val() || id,
                variation_id = $form.find('input[name=variation_id]').val() || 0;
 
        var data = {
            action: 'woocommerce_ajax_add_to_cart',
            product_id: product_id,
            product_sku: '',
            quantity: product_qty,
            variation_id: variation_id,
        };
 
        $(document.body).trigger('adding_to_cart', [$thisbutton, data]);
 
        $.ajax({
            type: 'post',
            url: wc_add_to_cart_params.ajax_url,
            data: data,
            beforeSend: function (response) {
                $thisbutton.removeClass('added').addClass('loading');
            },
            complete: function (response) {
                $thisbutton.addClass('added').removeClass('loading');
            },
            success: function (response) {
 
                if (response.error & response.product_url) {
                    window.location = response.product_url;
                    return;
                } else {
                    $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, $thisbutton]);
                }
            },
        });
 
        return false;
    });
})(jQuery);
</script>