<?php 
    $args = array(
    'post_type'             => 'product',
    'post_status'           => 'publish',
    'ignore_sticky_posts'   => 1,
    'posts_per_page'        => '12',
    'tax_query'             => array(
        array(
            'taxonomy'      => 'product_cat',
            'field' => 'term_id', //This is optional, as it defaults to 'term_id'
            'terms'         => 46,
            'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
        )
    )
);
$products = new WP_Query($args);?>
<div class="tab">
<?php
    while ($products->have_posts() ):$products->the_post();?>
    
      <button class="tablinks button" onclick="openCity(event, '<?php echo get_the_ID();?>')"><?php echo get_the_title();?></button>
      
<?php endwhile;?>
</div>