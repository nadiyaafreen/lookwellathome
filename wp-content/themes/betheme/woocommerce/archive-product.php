<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version 	3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

/**
 * Hook: woocommerce_before_main_content.
 *
 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
 * @hooked woocommerce_breadcrumb - 20
 * @hooked WC_Structured_Data::generate_website_data() - 30
 */
do_action( 'woocommerce_before_main_content' );

?>
<header class="woocommerce-products-header">
	<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>
		<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title(); ?></h1>
	<?php endif; ?>

	<?php
	/**
	 * Hook: woocommerce_archive_description.
	 *
	 * @hooked woocommerce_taxonomy_archive_description - 10
	 * @hooked woocommerce_product_archive_description - 10
	 */
	do_action( 'woocommerce_archive_description' );
	?>
</header>
<?php

if ( have_posts() ) {
 $terms = get_the_terms ( get_the_ID(), 'product_cat' );
 global $product;
    $term_id = $terms[0]->term_id;
 	if(($terms[0]->term_id == 53) || ($terms[0]->term_id == 153)):
	echo '<div class="shop-filters">';
		/**
		 * Hook: woocommerce_before_shop_loop.
		 *
		 * @hooked wc_print_notices - 10
		 * @hooked woocommerce_result_count - 20
		 * @hooked woocommerce_catalog_ordering - 30
		 */
		do_action( 'woocommerce_before_shop_loop' );
	echo '</div>';
	endif;
	$i=1;
	if(($terms[0]->term_id != 53) && ($terms[0]->term_id != 153) && $product->is_type( 'variable' )){
		
    $args = array(
    'post_type'             => 'product',
    'post_status'           => 'publish',
    'ignore_sticky_posts'   => 1,
    'posts_per_page'        => '12',
    'tax_query'             => array(
        array(
            'taxonomy'      => 'product_cat',
            'field' => 'term_id', //This is optional, as it defaults to 'term_id'
            'terms'         => $term_id,
            'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
		    )
		)
	);
	$products = new WP_Query($args);?>
	<div class="tab">
	<?php
	    while ($products->have_posts() ):$products->the_post();?>
	    
	      <button class="tablinks button" onclick="openCity(event, '<?php echo get_the_ID();?>')"><?php echo get_the_title();?></button>
	      
	<?php endwhile;?>
	</div>
	<?php wp_reset_postdata();	}
	woocommerce_product_loop_start();

	// WC < 3.3 backward compatibility
	if( version_compare( WC_VERSION, '3.3', '<' ) ){
		woocommerce_product_subcategories();
	}

	while ( have_posts() ) {
		the_post();

		/**
		 * Hook: woocommerce_shop_loop.
		 *
		 * @hooked WC_Structured_Data::generate_product_data() - 10
		 */

		do_action( 'woocommerce_shop_loop' );
		//$terms = get_the_terms ( get_the_ID(), 'product_cat' );
    
    	if(($terms[0]->term_id != 53) && ($terms[0]->term_id != 153) && $product->is_type( 'variable' )){
    		if($i==1){
    			echo "<div id=".get_the_ID()." class='entry-summary tabcontent' style='display:block;'>";
    		}else{
    			echo "<div id=".get_the_ID()." class='entry-summary tabcontent'>";
    		}
    		
    		wc_get_template_part( 'content', 'waxing' );
    		echo "</div>";
    		$i++;
    	}else{
    		wc_get_template_part( 'content', 'product' );
    	}
		
	}

	woocommerce_product_loop_end();

	/**
	 * Hook: woocommerce_after_shop_loop.
	 *
	 * @hooked woocommerce_pagination - 10
	 */
	do_action( 'woocommerce_after_shop_loop' );

} else {

	/**
	 * Hook: woocommerce_no_products_found.
	 *
	 * @hooked wc_no_products_found - 10
	 */
	do_action( 'woocommerce_no_products_found' );

}

/**
 * Hook: woocommerce_after_main_content.
 *
 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
 */
do_action( 'woocommerce_after_main_content' );

/**
 * Hook: woocommerce_sidebar.
 *
 * @hooked woocommerce_get_sidebar - 10
 */
do_action( 'woocommerce_sidebar' );

get_footer( 'shop' );
