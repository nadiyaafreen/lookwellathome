<ul id="myList">
<?php

	$product = new WC_Product_Variable( get_the_ID() );
    $variations = $product->get_available_variations();
    
    foreach ($variations as $variation) { 
    	$json = array($variation);
    	
?>
		<li>
		<form class="variations_form cart margin0" action="<?php the_permalink();?>" method="post" enctype="multipart/form-data" data-product_id="<?php echo get_the_id();?>" data-product_variations='<?php echo json_encode($json);?>'>
			<table class="new-cart-form">
				<tr>
					<?php foreach ($variation['attributes'] as $key => $value) {?>
						<td><?php echo $variation['attributes'][$key];?></td>
					<?php } ?>
					
					
					<td><?php echo $variation['price_html'];?></td>
					<td><button type="submit" class="single_add_to_cart_button button alt">Add to cart</button></td>
				</tr>
			</table>
			<input type="hidden" name="add-to-cart" value="<?php echo get_the_id();?>">
			<input type="hidden" name="product_id" value="<?php echo get_the_id();?>">
			<input type="hidden" name="variation_id" class="variation_id" value="<?php echo $variation['variation_id'];?>">
		</form>
	</li>
<?php } ?>
</ul>
<div id="loadMore">Load more Options</div>
<div id="showLess">Show less Options</div>
