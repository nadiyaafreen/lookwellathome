<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'lookwell_lookwellathome');

/** MySQL database username */
define('DB_USER', 'lookwell_usrlook');

/** MySQL database password */
define('DB_PASSWORD', 'lookWell');

/** MySQL hostname */
define('DB_HOST', 'mysql1006.mochahost.com');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '06jX17mNpg5qbdmzybseb3sRjzMA5HOOw05RoqgheY1xnyrd943sMAqKBfHTrSOx');
define('SECURE_AUTH_KEY',  'h4u8LJ5abXaZTpbujRPAlu7zgKSsWVVs8MYPz14XBacSjcZ3ehWU3AmYQJy4UNXz');
define('LOGGED_IN_KEY',    'QsBVdXxBtQAJnzyH9dzzKLDWOSXbriLnh3pVAiG37N5Nqj3ZdbbaZwh1yvj2SaeT');
define('NONCE_KEY',        'ZYdvsaZYGbcAIvUjntOulOzWwepWipD23ZAsCCFBga9LZCc8fVwgTX5sRjaLYuNt');
define('AUTH_SALT',        'eaGzhaIoTc0jOY45CXS2DOUcSkrJRbe8mooAhTkTJoUs4SkogpidcmwGJ1tNczrS');
define('SECURE_AUTH_SALT', 'bjbvU5Louh0ts9KcBPuEKQFmFHfDrMKqgiipBHiL7NS97szOf6ASP5kI8ERXrOsA');
define('LOGGED_IN_SALT',   'unDaCeGZbxalS7ml7ls37UyJjBdOn1PuTf4rjddr56lqcieLTvzBOhcrbULT1HXO');
define('NONCE_SALT',       'yjOy6kCTz6ZHJTtgYePsmvyXotAY4hxCkcGyJcwGIDEODlJUPPc0PVNC4MTjpHas');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);

define('ALLOW_UNFILTERED_UPLOADS', true);

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', true);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
