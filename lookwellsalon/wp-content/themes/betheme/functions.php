<?php
/**
 * Theme Functions
 *
 * @package Betheme
 * @author Muffin group
 * @link http://muffingroup.com
 */


define( 'THEME_DIR', get_template_directory() );
define( 'THEME_URI', get_template_directory_uri() );

define( 'THEME_NAME', 'betheme' );
define( 'THEME_VERSION', '20.9.5.8' );

define( 'LIBS_DIR', THEME_DIR. '/functions' );
define( 'LIBS_URI', THEME_URI. '/functions' );
define( 'LANG_DIR', THEME_DIR. '/languages' );

add_filter( 'widget_text', 'do_shortcode' );

add_filter( 'the_excerpt', 'shortcode_unautop' );
add_filter( 'the_excerpt', 'do_shortcode' );

add_image_size( 'offer-size', 300, 300, true );
/* ----------------------------------------------------------------------------
 * White Label
 * IMPORTANT: We recommend the use of Child Theme to change this
 * ---------------------------------------------------------------------------- */
defined( 'WHITE_LABEL' ) or define( 'WHITE_LABEL', false );


/* ----------------------------------------------------------------------------
 * Loads Theme Textdomain
 * ---------------------------------------------------------------------------- */
load_theme_textdomain( 'betheme',  LANG_DIR );	// frontend
load_theme_textdomain( 'mfn-opts', LANG_DIR );	// backend


/* ----------------------------------------------------------------------------
 * Loads the Options Panel
 * ---------------------------------------------------------------------------- */
if( ! function_exists( 'mfn_admin_scripts' ) )
{
	function mfn_admin_scripts() {
		wp_enqueue_script( 'jquery-ui-sortable' );
	}
}
add_action( 'wp_enqueue_scripts', 'mfn_admin_scripts' );
add_action( 'admin_enqueue_scripts', 'mfn_admin_scripts' );

require( THEME_DIR .'/muffin-options/theme-options.php' );


/* ----------------------------------------------------------------------------
 * Loads Theme Functions
 * ---------------------------------------------------------------------------- */

$theme_disable = mfn_opts_get( 'theme-disable' );

// Functions ------------------------------------------------------------------
require_once( LIBS_DIR .'/theme-functions.php' );

// Header ---------------------------------------------------------------------
require_once( LIBS_DIR .'/theme-head.php' );

// Menu -----------------------------------------------------------------------
require_once( LIBS_DIR .'/theme-menu.php' );
if( ! isset( $theme_disable['mega-menu'] ) ){
	require_once( LIBS_DIR .'/theme-mega-menu.php' );
}

// Muffin Builder -------------------------------------------------------------
require_once( LIBS_DIR .'/builder/fields.php' );
require_once( LIBS_DIR .'/builder/back.php' );
require_once( LIBS_DIR .'/builder/front.php' );

// Custom post types ----------------------------------------------------------
$post_types_disable = mfn_opts_get( 'post-type-disable' );

if( ! isset( $post_types_disable['client'] ) ){
	require_once( LIBS_DIR .'/meta-client.php' );
}
if( ! isset( $post_types_disable['offer'] ) ){
	require_once( LIBS_DIR .'/meta-offer.php' );
}
if( ! isset( $post_types_disable['portfolio'] ) ){
	require_once( LIBS_DIR .'/meta-portfolio.php' );
}
if( ! isset( $post_types_disable['slide'] ) ){
	require_once( LIBS_DIR .'/meta-slide.php' );
}
if( ! isset( $post_types_disable['testimonial'] ) ){
	require_once( LIBS_DIR .'/meta-testimonial.php' );
}

if( ! isset( $post_types_disable['layout'] ) ){
	require_once( LIBS_DIR .'/meta-layout.php' );
}
if( ! isset( $post_types_disable['template'] ) ){
	require_once( LIBS_DIR .'/meta-template.php' );
}

require_once( LIBS_DIR .'/meta-page.php' );
require_once( LIBS_DIR .'/meta-post.php' );

// Content --------------------------------------------------------------------
require_once( THEME_DIR .'/includes/content-post.php' );
require_once( THEME_DIR .'/includes/content-portfolio.php' );

// Shortcodes -----------------------------------------------------------------
require_once( LIBS_DIR .'/theme-shortcodes.php' );

// Hooks ----------------------------------------------------------------------
require_once( LIBS_DIR .'/theme-hooks.php' );

// Widgets --------------------------------------------------------------------
require_once( LIBS_DIR .'/widget-functions.php' );

require_once( LIBS_DIR .'/widget-flickr.php' );
require_once( LIBS_DIR .'/widget-login.php' );
require_once( LIBS_DIR .'/widget-menu.php' );
require_once( LIBS_DIR .'/widget-recent-comments.php' );
require_once( LIBS_DIR .'/widget-recent-posts.php' );
require_once( LIBS_DIR .'/widget-tag-cloud.php' );

// TinyMCE --------------------------------------------------------------------
require_once( LIBS_DIR .'/tinymce/tinymce.php' );

// Plugins --------------------------------------------------------------------
require_once( LIBS_DIR .'/class-love.php' );
require_once( LIBS_DIR .'/plugins/visual-composer.php' );

// WooCommerce specified functions
if( function_exists( 'is_woocommerce' ) ){
	require_once( LIBS_DIR .'/theme-woocommerce.php' );
}

// Disable responsive images in WP 4.4+ if Retina.js enabled
if( mfn_opts_get( 'retina-js' ) ){
	add_filter( 'wp_calculate_image_srcset', '__return_false' );
}

// Hide activation and update specific parts ----------------------------------

// Slider Revolution
if( ! mfn_opts_get( 'plugin-rev' ) ){
	if( function_exists( 'set_revslider_as_theme' ) ){
		set_revslider_as_theme();
	}
}

// LayerSlider
if( ! mfn_opts_get( 'plugin-layer' ) ){
	add_action( 'layerslider_ready', 'mfn_layerslider_overrides' );
	function mfn_layerslider_overrides() {
		// Disable auto-updates
		$GLOBALS['lsAutoUpdateBox'] = false;
	}
}

// Visual Composer
if( ! mfn_opts_get( 'plugin-visual' ) ){
	add_action( 'vc_before_init', 'mfn_vcSetAsTheme' );
	function mfn_vcSetAsTheme() {
		vc_set_as_theme();
	}
}

// Dashboard ------------------------------------------------------------------
if( is_admin() ){

	require_once LIBS_DIR .'/admin/class-mfn-api.php';
	require_once LIBS_DIR .'/admin/class-mfn-helper.php';
	require_once LIBS_DIR .'/admin/class-mfn-update.php';

	require_once LIBS_DIR .'/admin/class-mfn-dashboard.php';
	$mfn_dashboard = new Mfn_Dashboard();

	if( ! isset( $theme_disable['demo-data'] ) ){
		require_once LIBS_DIR .'/importer/class-mfn-importer.php';
	}

	require_once LIBS_DIR .'/admin/tgm/class-mfn-tgmpa.php';

	if( ! mfn_is_hosted() ){
		require_once LIBS_DIR .'/admin/class-mfn-status.php';
	}

	require_once LIBS_DIR .'/admin/class-mfn-support.php';
	require_once LIBS_DIR .'/admin/class-mfn-changelog.php';
}
//Distance calculation in checkout page 
require_once( LIBS_DIR .'/distance-calculation.php' );
//add_action('woocommerce_template_single_add_to_cart','woocommerce_template_single_add_to_cart_callback',25);
function woocommerce_template_single_add_to_cart_callback() { 
    global $product;
    $terms = get_the_terms ( get_the_ID(), 'product_cat' );
    
    if($terms[0]->term_id == 53 || $terms[0]->term_id == 153)
        return;
    $attributes = $product->get_attributes();
    
    if(empty($attributes))
        return;

    require_once(THEME_DIR.'/inc/default-cart.php');
    
    /*if($terms[0]->term_id == 46){
    	
    	require_once(THEME_DIR.'/inc/threding-waxing.php');
    }else{
    	require_once(THEME_DIR.'/inc/default-cart.php');
    }*/
}

add_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart_callback', 15 ); 
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);

add_action('wp_ajax_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');
add_action('wp_ajax_nopriv_woocommerce_ajax_add_to_cart', 'woocommerce_ajax_add_to_cart');
        
function woocommerce_ajax_add_to_cart() {

            $product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
            $quantity = empty($_POST['quantity']) ? 1 : wc_stock_amount($_POST['quantity']);
            $variation_id = absint($_POST['variation_id']);
            $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);
            $product_status = get_post_status($product_id);

            if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id) && 'publish' === $product_status) {

                do_action('woocommerce_ajax_added_to_cart', $product_id);

                if ('yes' === get_option('woocommerce_cart_redirect_after_add')) {
                    wc_add_to_cart_message(array($product_id => $quantity), true);
                }

                WC_AJAX :: get_refreshed_fragments();
            } else {

                $data = array(
                    'error' => true,
                    'product_url' => apply_filters('woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id));

                echo wp_send_json($data);
            }

            wp_die();
        }
function tatwerat_startSession() {
    if(!session_id()) {
        session_start();
        
    }
}

add_action('init', 'tatwerat_startSession', 1);

function footag_func( $atts ) {
    if(isset($_SESSION['user_location'])){
        $address = $_SESSION['user_location'];
    }else{
        $address = '';
    }
	return $address;
}
add_shortcode( 'user_location', 'footag_func' );
add_action('wp_ajax_set_user_address', 'set_user_address_callback');
add_action('wp_ajax_nopriv_set_user_address', 'set_user_address_callback');

function set_user_address_callback(){
    $adress = $_POST['address'];
    if($_SESSION['user_location'] !=''){
	
        $_SESSION['user_location'];
	
    }else{
    	
    	$_SESSION['user_location']=$adress;
    
    }
    
    echo json_encode(array("address"=>$adress));
    die();
}

//API file

require_once( THEME_DIR .'/inc/api.php' );

add_action('wp_ajax_custom_add_to_cart', 'custom_add_to_cart_callback');
add_action('wp_ajax_nopriv_custom_add_to_cart', 'custom_add_to_cart_callback');    

function custom_add_to_cart_callback(){
    //echo "ajax";
   
    if($_POST['services'] ==""){
        echo "<p>Please select at least one service.</p>";
        die();
    }
    if($_POST['services']):
       foreach($_POST['services'] as $service):
           //var_dump($service);
           $ser_arr = explode(",",$service);
           
            $product_id   =$ser_arr[0];
            $quantity     = 1;
            $variation_id = $ser_arr[1];
            $product_type = $ser_arr[2];
            if("vproduct" == $product_type ):
                $product = new WC_Product_Variable( $product_id );
                $variations = $product->get_available_variations();
                
                $cart_variation =array();
                foreach ($variations as $variation) {
                    if($variation['variation_id'] == $variation_id){
                        $attributes = get_post_meta($product_id,'_product_attributes',true);
            		    
            		    foreach($attributes as $key=>$value){
            		        $attribute_name = $key;
            		    } 
            		    $key = 'attribute_'.$attribute_name;
                        // print_r($variation['attributes'][$key]);
                        $cart_variation    = array(
                            $key=>$variation['attributes'][$key]
                            );
                    }
                }
                    
                WC()->cart->add_to_cart( $product_id, $quantity, $variation_id,$cart_variation );
            else:
                WC()->cart->add_to_cart( $product_id, $quantity);
            endif;
        endforeach;
    endif;
    echo "<p>All service has been added to cart Please Check <a href='/cart'>Cart</a></p>";
    die();
}

add_filter( 'woocommerce_states', 'bbloomer_custom_woocommerce_states' );
 
function bbloomer_custom_woocommerce_states( $states ) {
$states['IN'] = array(
'TS' => 'Telangana',
);
return $states;
}

add_filter( 'woocommerce_checkout_fields' , 'theme_override_checkout_notes_fields' );

// Our hooked in function - $fields is passed via the filter!
function theme_override_checkout_notes_fields( $fields ) {
     $fields['order']['order_comments']['placeholder'] = 'Leave your special instructions here..';
     $fields['order']['order_comments']['label'] = 'Special Instructions';
     return $fields;
}

add_filter( 'woocommerce_product_variation_title_include_attributes', '__return_true' );