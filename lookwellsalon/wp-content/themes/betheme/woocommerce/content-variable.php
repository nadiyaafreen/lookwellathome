<?php //do_action( 'woocommerce_before_single_product' );?>
<div  class="product">
	<h2 class="product-offer-name"><?php echo get_the_title();?></h2>
<ul class="myList1" id="myList1-<?php echo get_the_ID();?>" data-list="<?php echo get_the_ID();?>">
    	
<?php

	$product = new WC_Product_Variable( get_the_ID() );
    $variations = $product->get_available_variations();
    
    foreach ($variations as $variation) { 
    	$json = array($variation);
    	//print_r($variation['attributes']);
    	$product_info = array('product_id'=>get_the_ID(),'variation_id'=>$variation['variation_id'],'attribute'=>$variation['attributes']);
?>
		<li>
			<table>
			    <tr>
			        <td>
			            <input class="producttocart" type="checkbox" name="service" value="<?php echo get_the_ID().','.$variation['variation_id'].',vproduct';?>" />
			            <span class="checkmark"></span>
			        </td>
				    <td width="200px">
				
					<?php foreach ($variation['attributes'] as $key => $value) {?>
						<span class="font-bold"><?php echo $variation['attributes'][$key];?></span>
					<?php } ?>
					</td>
				    <!--<td><i class="icon-arrow-right"></i></td>-->
				    <td class="p-0"><span><?php echo $variation['price_html'];?></span></td>
		       
			</tr>
			</table>
	
	</li>
<?php } ?>
</ul>
</div>

<?php
//do_action( 'woocommerce_after_single_product' ); ?>
