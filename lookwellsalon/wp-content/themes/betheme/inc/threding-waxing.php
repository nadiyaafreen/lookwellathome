<h2>Please Select Any Service</h2>
<?php
    $i=1;
    foreach ($attributes as $attribute) { 
        if($i==1){
            $lenght = count($attribute->get_options());
            $all_att = $attribute->get_options();
            for($j=0;$j<$lenght;$j++){?>
                <a class="button alt button-margin" href="<?php the_permalink();?>?service_type=<?php echo str_replace(" ","_",$all_att[$j]);?>"><?php echo $all_att[$j];?></a>
        <?php }
        }
        $i++;
    }
    $service_type= $_GET['service_type'];
    
    if($service_type =="")
        return;
    $service_type=str_replace("_"," ",$service_type);
    $product = new WC_Product_Variable( get_the_ID() );
    $variations = $product->get_available_variations();

    foreach ($variations as $variation) { $json = array($variation);
    if(in_array($service_type,$variation['attributes'])){
?>
	
	<form class="variations_form cart" action="<?php the_permalink();?>" method="post" enctype="multipart/form-data" data-product_id="<?php echo get_the_id();?>" data-product_variations='<?php echo json_encode($json);?>'>
		<table class="new-cart-form">
			<tr>
				<?php foreach ($variation['attributes'] as $key => $value) {?>
					<td><?php echo $variation['attributes'][$key];?></td>
				<?php } ?>
				
				
				<td><?php echo $variation['price_html'];?></td>
				<td><button type="submit" class="single_add_to_cart_button button alt">Add to cart</button></td>
			</tr>
		</table>
		<input type="hidden" name="add-to-cart" value="<?php echo get_the_id();?>">
		<input type="hidden" name="product_id" value="<?php echo get_the_id();?>">
		<input type="hidden" name="variation_id" class="variation_id" value="<?php echo $variation['variation_id'];?>">
	</form>
<?php } } 