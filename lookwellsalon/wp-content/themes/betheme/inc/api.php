<?php
add_action( 'rest_api_init', function () {
	register_rest_route( 'api/v2', '/login/', array(
	   'methods' => 'POST',
	   'callback' => 'user_login',
	   'args' => [
	        'email',
	        'password'
	    ],
	) );
} );


function user_login($data){
	
	$responce['responce_code'] ='';
	$responce['message']='';
	$responce['error']=false;
	$responce['success']=true;

	if($data['email']==null || $data['password'] ==null){
		$responce['responce_code'] =400;
		$responce['message']='Missing login details';
		$responce['error']=true;
		$responce['success']=false;
		return $responce;
	}
	
	$creds['user_login'] = $data['email'];
	$creds['user_password'] = $data['password'];
	$creds['remember'] = true;
	$user = wp_signon( $creds, false );
	if ( is_wp_error($user) ){
		$responce['responce_code'] =400;
		$responce['message']='Invalid Credentails.';
		$responce['error']=true;
		$responce['success']=false;
		return $responce;
		
	}else{
		$responce['responce_code'] =200;
		$responce['message']='Login successfully.';
		$responce['error']=false;
		$responce['success']=true;
		return $responce;
	}
	
	

}

add_action( 'rest_api_init', function () {
	register_rest_route( 'api/v2', '/signup/', array(
	   'methods' => 'POST',
	   'callback' => 'create_user_account',
	   'args' => [
	        'email',
	        'first_name',
	        'last_name',
	        'password'
	        
	    ],
	) );
} );

function create_user_account($data){
	$user_name = $data['first_name'].$data['last_name'];
	$user_email = $data['email'];
	$user_id = username_exists( $user_name );
	if($data['first_name'] == null || $data['last_name'] == null || $data['email'] ==null || $data['password'] == null){
		$responce['responce_code'] =400;
		$responce['message']='Missing userdata.';
		$responce['error']=true;
		$responce['success']=false;
		return $responce;
	}
	if ( !$user_id and email_exists($user_email) == false ) {
		$userdata = array(
	    'user_login'  =>  $user_name,
	    'user_email'  =>$data['email'],
	    'user_pass'   =>   $data['password'] ,
	    'first_name'  => $data['first_name'],
	    'last_name'   => $data['last_name'],
	    'role'=>'subscriber'
		);

		$user_id = wp_insert_user( $userdata ) ;

		//On success
		if ( ! is_wp_error( $user_id ) ) {
		   $responce['responce_code'] =200;
			$responce['message']='Account created successfully.';
			$responce['error']=false;
			$responce['success']=true;
			return $responce;
		}else{
			$responce['responce_code'] =400;
			$responce['message']='Some thing went wrong.';
			$responce['error']=true;
			$responce['success']=false;
			return $responce;
		}
	} else {
		$responce['responce_code'] =400;
		$responce['message']='User already exists.';
		$responce['error']=true;
		$responce['success']=false;
		return $responce;
	}

}


function api_send_password_reset_mail($user_id){

    $user = get_user_by('id', $user_id);
    $firstname = $user->first_name;
    $email = $user->user_email;
    $adt_rp_key = get_password_reset_key( $user );
    $user_login = $user->user_login;
    $rp_link = '<a href="' . wp_login_url()."?action=rp&key=$adt_rp_key&login=" . rawurlencode($user_login) . '">' . wp_login_url()."?action=rp&key=$adt_rp_key&login=" . rawurlencode($user_login) . '</a>';

    if ($firstname == "") $firstname = "gebruiker";
    $message = "Hi ".$firstname.",<br>";
    $message .= "An account has been created on ".get_bloginfo( 'name' )." for email address ".$email."<br>";
    $message .= "Click here to reset the password for your account: <br>";
    $message .= $rp_link.'<br>';

    //deze functie moet je zelf nog toevoegen. 
   $subject = __("Your account on ".get_bloginfo( 'name'));
   $headers = array();
   
   add_filter( 'wp_mail_content_type', function( $content_type ) {return 'text/html';});
   $headers[] = 'From: Look Well at Home <info@lookwellathome.com>'."\r\n";
   wp_mail( $email, $subject, $message, $headers);

   // Reset content-type to avoid conflicts -- http://core.trac.wordpress.org/ticket/23578
   remove_filter( 'wp_mail_content_type', 'set_html_content_type' );

  return true;
}



add_action( 'rest_api_init', function () {
	register_rest_route( 'api/v2', '/reset-password/', array(
	   'methods' => 'POST',
	   'callback' => 'reset_password_link',
	   'args' => [
	        'email'
	        
	    ],
	) );
} );

function reset_password_link($data){

	if($data['email'] == null){
		$responce['responce_code'] =400;
		$responce['message']='Please enter email id';
		$responce['error']=true;
		$responce['success']=false;
		return $responce;
	}
	$user_email = $data['email'];
	if(email_exists($user_email)){
		$user = get_user_by( 'email', $user_email );
		$user_id = $user->ID;
		$send = api_send_password_reset_mail($user_id);
		if($send){
			$responce['responce_code'] =200;
			$responce['message']='Please check your email for password reset link.';
			$responce['error']=false;
			$responce['success']=true;
			return $responce;
		}else{
			$responce['responce_code'] =400;
			$responce['message']='Something went wrong while sending reset password link.';
			$responce['error']=true;
			$responce['success']=false;
			return $responce;
		}
	}else{
		$responce['responce_code'] =400;
		$responce['message']='Email ID does not exists.';
		$responce['error']=true;
		$responce['success']=false;
		return $responce;
	}
}