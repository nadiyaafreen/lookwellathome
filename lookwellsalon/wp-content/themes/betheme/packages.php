<?php
/*
Template Name:New Packages

*/
get_header('shop');
?>

<div class="container">
	<div class="woocommerce-before-loop">
		<?php do_action( 'woocommerce_before_shop_loop' );?>
	</div>
</div>
<div class="container">
	<div class="row marginb50 center">
		<img src="https://www.lookwellathome.com/wp-content/uploads/2019/08/ki-300x57.png" />
	</div>
</div>

<form method="post" id="cart-from">
<div>
<div class="container service-cat service-active" id="offers">
    <div class="row">
	<ul class="products">  
		    <?php
		    $i=1;
		   wp_reset_postdata();?>
		    <?php get_template_part('templates/package');?>
		    <?php wp_reset_query(); ?>
		    
		</ul>
		</div>		

	
</div>

<div class="container">
    <div class="row">
        <div class="notice"></div>
        <div class="loader text-center">
            <img src="<?php echo get_template_directory_uri();?>/images/loader.gif" />
        </div>
        <button id="addtocart2" class="btn btn-primary" >Add To Cart</button>
        
    </div>
    
</div>

</div>

</form>
<div class="container">
	<div class="row center">
		<img src="https://www.lookwellathome.com/wp-content/uploads/2019/08/kisspn-300x41.png" />
	</div>
</div>

<?php
get_footer('shop');
?>
<script type="text/javascript">
  jQuery.noConflict();
  jQuery(document).ready(function(){
    jQuery('.toggle').click(function(e) {
    e.preventDefault();
  
    var $this = jQuery(this);
    var id = $this.data("id");
    var newid = "#accordion-"+id;
    // jQuery('.inner').removeClass('show');
    // jQuery('.inner').slideUp(350);
   
        jQuery( ".inner" ).each(function() {
          if(jQuery(newid).hasClass('show1')){
              
          }else{
                jQuery(this).removeClass('show1');
        
                jQuery(this).slideUp(350);
          }
        });
    if (jQuery(newid).hasClass('show1')) {
        jQuery(newid).removeClass('show1');
        jQuery(newid).slideUp(350);
    } else {
        jQuery(newid).addClass('show1');
        
        jQuery(newid).slideDown(350);
    }
  });
  jQuery('.see-service').click(function(e) {
    e.preventDefault();
    
    var $this = jQuery(this);
    var id = jQuery(this).data("catid");
    var newid = "#"+id;
    
      jQuery('.see-service').removeClass('active-service')
      jQuery('.service-cat').addClass('service-inactive');
      jQuery($this).addClass('active-service')
      
    //   jQuery('.service-cat').removeClass('service-inactive');
    //   if(jQuery('.service-cat').hasClass('service-active')){
    //     jQuery('.service-cat').removeClass('service-active');
    //     jQuery('.service-cat').addClass('service-inactive');
    //   }
      jQuery(newid).removeClass('service-inactive');
      jQuery(newid).addClass('service-active');
    
  });
  })
</script>
<script>
    jQuery(document).ready(function(){
   jQuery(document).on('submit', '#cart-from', function (e) {
        e.preventDefault();
 
        var chkArray = [];
	    jQuery(".loader").show();
    	var services = [];
        jQuery.each(jQuery("input[name='service']:checked"), function(){            
           //console.log(jQuery(this).val());
           services.push(jQuery(this).val());
           chkArray.push(jQuery(this).data('attribute'));
        });
    	
        jQuery.ajax({
            
            type: "POST",
            url: "/wp-admin/admin-ajax.php",
            data: { 
                att:chkArray,
                services:services,
                action: 'custom_add_to_cart'
                
            },
           
            success: function(data){
               
                jQuery(".notice").html(data);
                jQuery(".loader").hide();
                jQuery("#cart-from").trigger("reset");
            }
        });
       
    });
});
</script>