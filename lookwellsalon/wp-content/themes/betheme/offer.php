<?php
/*
Template Name: New Offers

*/
get_header('shop');
?>

<div class="container">
	<div class="woocommerce-before-loop">
		<?php do_action( 'woocommerce_before_shop_loop' );?>
	</div>
</div>
<div class="container">
	<div class="row marginb50 center">
		<img src="https://lookwellsalon.co.in/wp-content/uploads/2014/11/ki-300x57.png" />
	</div>
</div>

<form method="post" id="cart-from">
<div>
<div class="container service-cat service-active" id="offers">
    <div class="row">
	<ul class="products">  
		    <?php
		   wp_reset_postdata();
		    $i=1;
		        $args1 = array( 'post_type' => 'product','posts_per_page' => -1, 'post__in' => array(4271,5045,894,4113,4138,4280,4487,4495),'orderby'=>'post__in' );
		        $loop = new WP_Query( $args1 );
		        while ( $loop->have_posts() ) : $loop->the_post(); global $product; ?>
		   
		                <li class="product mcb-column one-third column <?php echo $i;?> margin-60"> 
		                <div class="accordion">   
		                    <a class="toggle" href="javascript:void(0);" data-id="<?php echo get_the_ID();?>">
		                        <?php woocommerce_show_product_sale_flash( $post, $product ); ?>
		                        <?php 
									echo woocommerce_get_product_thumbnail('offer-size');
									?>
		                        <h3 class="service-name"><?php the_title(); ?></h3>
		                                            
		                    </a>
		                    
		                    <div class="inner" id="accordion-<?php echo get_the_ID();?>">
								
								<?php
								$product_id = get_the_ID(); // the ID of the product to check
                                $_product = wc_get_product( $product_id );
                                if( $_product->is_type( 'simple' ) ) {
                                    wc_get_template_part( 'content', 'single_service' );
                                } else {
                                
                                 wc_get_template_part( 'content', 'variable' );
                                }
                                ?>
							</div>
						</div>
		                </li>
		    
		    <?php $i++;endwhile; ?>
		    <?php wp_reset_query(); ?>
		    
		</ul>
		</div>		

	
</div>

<div class="container">
    <div class="row">
        <div class="notice"></div>
        <div class="loader text-center">
            <img src="<?php echo get_template_directory_uri();?>/images/loader.gif" />
        </div>
        <button id="addtocart2" class="btn btn-primary" >Add To Cart</button>
        
    </div>
    
</div>

</div>

</form>
<div class="container">
	<div class="row center">
		<img src="https://lookwellsalon.co.in/wp-content/uploads/2014/11/kisspn-300x41.png" />
	</div>
</div>

<?php
get_footer('shop');
?>
<script type="text/javascript">
  jQuery.noConflict();
  jQuery(document).ready(function(){
    jQuery('.toggle').click(function(e) {
    e.preventDefault();
  
    var $this = jQuery(this);
    var id = $this.data("id");
    var newid = "#accordion-"+id;
    // jQuery('.inner').removeClass('show');
    // jQuery('.inner').slideUp(350);
   
        jQuery( ".inner" ).each(function() {
          if(jQuery(newid).hasClass('show1')){
              
          }else{
                jQuery(this).removeClass('show1');
        
                jQuery(this).slideUp(350);
          }
        });
    if (jQuery(newid).hasClass('show1')) {
        jQuery(newid).removeClass('show1');
        jQuery(newid).slideUp(350);
    } else {
        jQuery(newid).addClass('show1');
        
        jQuery(newid).slideDown(350);
    }
  });
  jQuery('.see-service').click(function(e) {
    e.preventDefault();
    
    var $this = jQuery(this);
    var id = jQuery(this).data("catid");
    var newid = "#"+id;
    
      jQuery('.see-service').removeClass('active-service')
      jQuery('.service-cat').addClass('service-inactive');
      jQuery($this).addClass('active-service')
      
    //   jQuery('.service-cat').removeClass('service-inactive');
    //   if(jQuery('.service-cat').hasClass('service-active')){
    //     jQuery('.service-cat').removeClass('service-active');
    //     jQuery('.service-cat').addClass('service-inactive');
    //   }
      jQuery(newid).removeClass('service-inactive');
      jQuery(newid).addClass('service-active');
    
  });
  })
</script>
<script>
    jQuery(document).ready(function(){
   jQuery(document).on('submit', '#cart-from', function (e) {
        e.preventDefault();
 
        var chkArray = [];
	    jQuery(".loader").show();
    	var services = [];
        jQuery.each(jQuery("input[name='service']:checked"), function(){            
           //console.log(jQuery(this).val());
           services.push(jQuery(this).val());
           chkArray.push(jQuery(this).data('attribute'));
        });
    	
        jQuery.ajax({
            
            type: "POST",
            url: "/wp-admin/admin-ajax.php",
            data: { 
                att:chkArray,
                services:services,
                action: 'custom_add_to_cart'
                
            },
           
            success: function(data){
               
                jQuery(".notice").html(data);
                jQuery(".loader").hide();
                jQuery("#cart-from").trigger("reset");
            }
        });
       
    });
});
</script>