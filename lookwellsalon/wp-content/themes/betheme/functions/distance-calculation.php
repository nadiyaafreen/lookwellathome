<?php
add_filter( 'woocommerce_checkout_fields' , 'lookwell_print_all_fields' );
 
function lookwell_print_all_fields( $fields ) {
 
	
	$fields['billing']['billing_address_1']['required'] = true;
	$fields['billing']['billing_city']['required'] = true;
	return $fields;
}

add_action( 'woocommerce_after_checkout_validation', 'lookwell_validate_adress_field', 10, 2);
 
function lookwell_validate_adress_field( $fields, $errors ){
    $apiKey = "AIzaSyAInfR37Vbbffd6hsdI4PHMXjq-oC3AGK0";
    $latitudeFrom    = 17.410714;
    $longitudeFrom  = 78.464557;
    $cart_total = WC()->cart->cart_contents_total;
    if ($fields['billing_city'] !=""){
        
        $to_address = $fields['billing_address_1'].' '.$fields['billing_address_2'].' '.$fields['billing_city'].' '.$fields['billing_state'].' '.$fields['billing_postcode'];
        $formattedAddrTo     = str_replace(' ', '+', $to_address);
        $geocodeTo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddrTo.'&sensor=false&key='.$apiKey);
        $outputTo = json_decode($geocodeTo);
        if(!empty($outputTo->error_message)){
            $errors->add( 'validation', $outputTo->error_message );
            
        }
        $latitudeTo        = $outputTo->results[0]->geometry->location->lat;
        $longitudeTo    = $outputTo->results[0]->geometry->location->lng;
        // Calculate distance between latitude and longitude
        $theta    = $longitudeFrom - $longitudeTo;
        $dist    = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) +  cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
        $dist    = acos($dist);
        $dist    = rad2deg($dist);
        $miles    = $dist * 60 * 1.1515;
        $distance = round($miles * 1.609344, 2);
        global $woocommerce;
        $amount2 = floatval( preg_replace( '#[^\d.]#', '', $woocommerce->cart->get_cart_total() ) );
        if($distance >=5 && $distance <= 7 && $cart_total <999){
            $errors->add( 'validation', "Your cart total is less than Rs 999 Please ensure cart total must be greater than Rs 999");
        }else if($distance >=7 && $distance <=10 && $cart_total <1500){
            $errors->add( 'validation', "Your cart total is less than Rs 1500 Please ensure, cart total must be greater than Rs 1500");
        }else if($distance >=10 && $distance <=15 && $cart_total <2000){
            $errors->add( 'validation', "Your cart total is less than Rs 2000 Please ensure, cart total must be greater than Rs 2000");
        }else if($distance >=15 && $cart_total <2500){
            $errors->add( 'validation', "Your cart total is less than Rs 2500 Please ensure, cart total must be greater than Rs 2500");
        }
        
    }else{
        $errors->add( 'validation', "Please Enter City");
    }
}
?>