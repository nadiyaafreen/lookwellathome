<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
} ?>

<div class="wrap">
	<h2><?php _e('WooCommerce Easy Booking Add-ons'); ?></h2>
	<div class="addons-container row">
		<?php $addons = array(
			array(
				'name' => 'Availability Check',
				'slug' => 'availability-check',
				'desc' => '<p>'
					. __( 'Manage availabilities of your bookable products.', 'woocommerce-easy-booking-system' ) .
				'</p>'
			),
			array(
				'name' => 'Duration Discounts',
				'slug' => 'duration-discounts',
				'desc' => '<p>
					' .  __( 'Set discounts or surcharges depending on booking durations.', 'woocommerce-easy-booking-system' ) . '
				</p>'
			)
			,
			array(
				'name' => 'Disable Dates',
				'slug' => 'disable-dates',
				'desc' => '<p>
					' .  __( 'Disable days or dates on your products booking schedules.', 'woocommerce-easy-booking-system' ) . '
				</p>'
			)
			,
			array(
				'name' => 'Pricing',
				'slug' => 'pricing',
				'desc' => '<p>
					' .  __( 'Set different prices depending on a day, date or daterange.', 'woocommerce-easy-booking-system' ) . '
				</p>'
			)
		);

		$plugin_dir = plugins_url( '/', WCEB_PLUGIN_FILE );

		$active_plugins = (array) get_option( 'active_plugins', array() );

        if ( is_multisite() ) {
            $active_plugins = array_merge( $active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );
        }

		foreach ( $addons as $addon ) : ?>
			<div class="addon-single">
				<div class="addon-single__img">
					<img src="<?php echo $plugin_dir . 'assets/img/addons/' . $addon['slug'] . '.png'; ?>" alt="<?php echo $addon['slug']; ?>">
				</div>
				<div class="addon-single__desc">
					<h2><?php echo $addon['name']; ?></h2>
					<?php echo $addon['desc']; ?>
					<p>
						<?php if ( ! ( array_key_exists( 'easy-booking-' . $addon['slug'] .'/' . 'easy-booking-' . $addon['slug'] . '.php', $active_plugins ) || in_array( 'easy-booking-' . $addon['slug'] .'/' . 'easy-booking-' . $addon['slug'] . '.php', $active_plugins ) ) ) { ?>
						<a href="https://www.easy-booking.me/addon/<?php echo $addon['slug']; ?>" target="_blank" class="button">
							<?php _e('Learn more', 'woocommerce-easy-booking-system'); ?>
						</a>
						<?php } else { ?>
						<a href="#" class="button easy-booking-button easy-booking-button--installed">
							<?php _e('Installed', 'woocommerce-easy-booking-system'); ?>
						</a>
						<a href="https://www.easy-booking.me/documentation/<?php echo $addon['slug']; ?>" target="_blank" class="button">
							<?php _e('Documentation', 'woocommerce-easy-booking-system'); ?>
						</a>
						<a href="https://www.easy-booking.me/support/<?php echo $addon['slug']; ?>" target="_blank" class="button">
							<?php _e('Support', 'woocommerce-easy-booking-system'); ?>
						</a>
						<?php } ?>
					</p>
				</div>
			</div>

		<?php endforeach; ?>
	</div>
</div>